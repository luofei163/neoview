#!/bin/bash
LIST="libusb1 eveusb neoview-linux neo-remote-viewer usb-share"
CUR_DIR=`pwd`
down_code()
{
CODE=$1
BRANCH="release-v2.6"
cd tmp
git clone ssh://fei.luo@10.1.110.123:29418/$CODE.git
cd $CODE
git checkout -b $BRANCH origin/$BRANCH
sh build.sh
cd $CUR_DIR

}
rm -rf rpmbuild
rpmdev-setuptree
rm -rf tmp
mkdir tmp
for x in $LIST
do
	rm -rf $x
	down_code $x
done
rm -rf SRPMS
cp -rf  ~/rpmbuild/SRPMS .


