'''
Created on Aug 15, 2013

@author: Luo Fei
'''

import os
import subprocess
import shutil
import time
from time import sleep
import sys
import logging
import zipfile 
import  ConfigParser
from xml.dom import  minidom

logger = logging.getLogger("buildneoview")
hdlr = logging.FileHandler('build_neoview.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)

stream_hdlr = logging.StreamHandler()
logger.addHandler(stream_hdlr)

logger.setLevel(logging.DEBUG)

CURDIR=os.getcwd()

# Exceptions
class GenericError(Exception):
    """Base class for our custom exceptions"""
    faultCode = 1000
    fromFault = False
    def __str__(self):
        try:
            return str(self.args[0]['args'][0])
        except:
            try:
                return str(self.args[0])
            except:
                return str(self.__dict__)
            
class RunCommandError(GenericError):
    """Build rpm package exceptions"""
    faultCode = 1001
    


def get_xmlnode(node,name):
    return node.getElementsByTagName(name) if node else []

def get_attrvalue(node, attrname):
    return node.getAttribute(attrname) if node else ''

def set_attrvalue(node,attr,value):
    node.setAttribute(attr,value)  
 
def clean(dir_path):
    for root,dirs,files in os.walk(dir_path):
   
        for dir in dirs:
            args = ['Win32','x64','Debug','Release']
            for arg in args:
                if dir.startswith(arg):
                    shutil.rmtree(os.path.join(root,dir))
        for file in files:
            if file.endswith('.exe') and not file.startswith('devcon.exe'):
                os.remove(os.path.join(root,file))


    
def modify_value(project,match,attr,new_value):
    root = minidom.parse(project)       
    all =   root.documentElement
    vclinker_signal = False
    vcompiler_signal = False
    for node in   get_xmlnode(all,'Configuration'):
        if get_attrvalue(node, 'Name') == match:
            for subnode in get_xmlnode(node,'Tool'):
                
                value = get_attrvalue(subnode,attr)
                if get_attrvalue(subnode,'Name') == 'VCLinkerTool':
                    set_attrvalue(subnode,attr,new_value)
#                     print get_attrvalue(subnode,attr)
                    vclinker_signal = True

                if get_attrvalue(subnode,'Name') == 'VCCLCompilerTool':
                    set_attrvalue(subnode,attr,new_value)
#                     print get_attrvalue(subnode,attr)
                    vcompiler_signal = True
            
    with open(project,'w') as f:
        f.write(root.toxml('utf-8'))
        
def __init_environment():
    global  DEVENV,PROJECT_DIR,RESOURCE,WINDDK,ZIP_CONTENTS,BOOT,WIN32_LIB,WIN64_LIB
    config = ConfigParser.RawConfigParser()
    config.read('neoview.conf')
    DEVENV = config.get('build', 'devenv')
    PROJECT_DIR = os.path.join(CURDIR,config.get('build', 'base_dir'))
    RESOURCE = os.path.join(CURDIR,config.get('build', 'resource_dir'))
    WINDDK = config.get('build', 'winddk')
    ZIP_CONTENTS = config.get('zip','contents')
    BOOT = config.get('build', 'boot')
    WIN32_LIB = config.get('build','win32_lib')
    WIN64_LIB = config.get('build','win64_lib')



def run_commands(cmds, logger):
    run_cmds = []
    if isinstance(cmds, list):
        run_cmds.extend(cmds)
    else:
        run_cmds.append(cmds)
            
    for cmd in run_cmds:
        logger.info(os.linesep)
        project_name = cmd.split(' ')[-1]
        logger.info('Begin build %s' % project_name)
        if run_command(cmd, logger) is True:
            logger.info('Finished build %s' % project_name)
        else:
            logger.info('Build error: %s' % project_name)
        logger.info(os.linesep)
        
def run_command(cmd, logger):
    '''Execute cmd'''
    sign = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    logger.info(cmd)
    while sign.poll() == None:
        out = sign.stdout.readline()
#             print out
#             logger.info(out)
    sign.wait()
    if sign.returncode != 0:
        logger.error(sign.returncode)
        raise RunCommandError,'Run command failed.'
#         if exit == True:
#         sys.exit(1)
        return False
    else:

        return True

def copy_file(src, dist, suffix):
 
    for path, dirs, files in os.walk(src):
        for file in files:
            if file.endswith(suffix):
                src = os.path.join(path, file)
                shutil.copy(src, dist)
                logger.info('copy %s to %s successfull.' % (src,dist))

def copy_restart_exe(src,dist):
    restart_file = os.path.isfile(os.path.join(dist,'RestartComputer.exe'))
    unit_file = os.path.join(os.path.dirname(os.path.dirname(dist)),'UnInstall','Resources')
    if restart_file and not os.path.isfile(os.path.join(unit_file,'RestartComputer.exe')):
        shutil.copy(os.path.join(src,'RestartComputer.exe'), unit_file)
    
def build(func):
    def __build(**args):
        try:
            os.chdir(args['project_path'])
            cmds = func()
            run_commands(cmds, args['logger'])
            copy_file(args['src'], args['dist'], args['suffix'])
        except Exception, ex:
            logger.exception(ex)
    return __build


@build   
def build_spice():
    project = ['RestartComputer','NeoConnector', 'NeoView']
    cmds = []
    for pj in project:
        cmd = '"%s" Spice.sln /Rebuild "release|x86" /project %s' % (DEVENV, pj)
        cmds.append(cmd)
    return cmds

@build
def build_uninstall():
    cmd = '"%s" Spice.sln /Rebuild "release|x86" /project %s' % (DEVENV, 'UnInstall')
    return cmd

@build                      
def build_redc():  
    cmd = '"%s" redc.sln /Rebuild release /project %s' % (DEVENV, 'redc') 
    return cmd


@build
def build_usb_service():
    cmds = []
    for x in ['Win32', 'x64']:
        cmd = '"%s" UsbConfig.sln /Rebuild  "release|%s" /project %s' % (DEVENV, x, 'UsbService') 
        cmds.append(cmd)
    return cmds

@build 
def build_u2eclib():
    for x in ['Win32','x64']:
        cmd = '"%s" UsbConfig.sln /Rebuild  "release|%s" /Project %s' % (DEVENV, x, 'u2ec') 
    return cmd

@build 
def build_u2ecdll():
    for x in ['Win32']:
        cmd = '"%s" UsbConfig.sln /Rebuild  "release dll|%s" /project %s' % (DEVENV, x, 'u2ec') 
    return cmd
    
def build_driver(base_dir, winddk, resource):
    """
        WXP: windows xp
        wnet: windows 2003
    """
    
  
    dirs = []
    cmds = []
    nt6_x86 = os.path.join(resource, 'drv','NT6','x86')
    nt6_x64 = os.path.join(resource, 'drv','NT6','x64')
    nt5_x86 = os.path.join(resource, 'drv','NT5','x86')   
    # Copy inf to dist                      
    inf_dir = os.path.join(base_dir,'Inf')
    for d in [nt6_x86,nt6_x64,nt5_x86]:
        copy_file(inf_dir,d,'.inf')
    # Create .sys file
    for dir in ['evuh', 'fusbhub', 'usbstub']:
        dir_path = os.path.join(base_dir, dir)
        dirs.append(dir_path)
        os.chdir(dir_path)
        systems = {'win7':['x86', 'x64'],
                  'WXP':'x86',
                  'wnet':'x86'}
        for system in systems:
            arch = systems.get(system)
            if isinstance(arch, list):
                for a in arch:
                    cmd = r'%s fre %s %s && cd /d  %s && build' % (winddk, a, system, dir_path)
                    print cmd
                    cmds.append(cmd)
            else:       
                cmd = r'%s fre %s %s && cd /d  %s && build' % (winddk, arch, system, dir_path)
                cmds.append(cmd)
    for c in cmds:
        run_command(c, logger)
       
    targets = []
    for d in dirs:
        for path, dirs, files in os.walk(d):
            for di in dirs:
                target = os.path.join(path, di)
                   
                if di.startswith('i386') and di.endswith('i386'):
                    if target.find('win7') != -1:
                        copy_file(target, nt6_x86, '.sys')
                         
                    elif target.find('wnet') != -1:
                        copy_file(target, nt5_x86, '.sys')
                    elif target.find('wxp') != -1:
                        copy_file(target, nt5_x86, '.sys')
                elif di.startswith('amd64') and di.endswith('amd64'):
                    if target.find('win7') != -1:
                        copy_file(target, nt6_x64, '.sys')  
 
    # Create cat file
    arg = '/os:XP_x86,XP_x64,Server2003_X86,Server2003_X64,Vista_X86,Vista_X64,Server2008_X86,Server2008_X64,7_X86,7_X64,Server2008R2_X64'
    cmd = r'%s fre %s %s && cd /d  %s &&  inf2cat /driver:. %s' % (winddk, 'x86', 'win7',  nt6_x86, arg)   
    cmd1 = r'%s fre %s %s && cd /d  %s && inf2cat /driver:. %s' % (winddk, 'x86', 'wxp',  nt5_x86, arg)   
    cmd2 = r'%s fre %s %s && cd /d  %s && inf2cat /driver:. %s' % (winddk, 'x64', 'wnet',  nt6_x64, arg)   
     
    run_commands([cmd,cmd1,cmd2],logger)
    
    
    
def build_driver_old(base_dir, winddk, resource):
    """
        WXP: windows xp
        wnet: windows 2003
    """
    
  
    dirs = []
    cmds = []
    for dir in ['evuh', 'fusbhub', 'usbstub']:
        dir_path = os.path.join(base_dir, dir)
        dirs.append(dir_path)
        os.chdir(dir_path)
        systems = {'win7':['x86', 'x64'],
                  'WXP':'x86',
                  'wnet':'x86'}
        for system in systems:
#              arches.get(arch)
            arch = systems.get(system)
            if isinstance(arch, list):
                for a in arch:
                    cmd = r'%s fre %s %s && cd /d  %s && build' % (winddk, a, system, dir_path)
                    print cmd
                    cmds.append(cmd)
            else:       
                cmd = r'%s fre %s %s && cd /d  %s && build' % (winddk, arch, system, dir_path)
                cmds.append(cmd)
    for c in cmds:
        run_command(c, logger)
    
    nt6_x86 = os.path.join(resource, r'drv\NT6\x86')
    nt6_x64 = os.path.join(resource, r'drv\NT6\x64')
    nt5_x86 = os.path.join(resource, r'drv\NT5\x86')
    
    targets = []
    for d in dirs:
        for path, dirs, files in os.walk(d):
            for di in dirs:
                target = os.path.join(path, di)
                
                if di.startswith('i386') and di.endswith('i386'):
                    if target.find('win7') != -1:
                        copy_file(target, nt6_x86, '.sys')
                      
                    elif target.find('wnet') != -1:
                        copy_file(target, nt5_x86, '.sys')
                    elif target.find('wxp') != -1:
                        copy_file(target, nt5_x86, '.sys')
                elif di.startswith('amd64') and di.endswith('amd64'):
                    if target.find('win7') != -1:
                        copy_file(target, nt6_x64, '.sys')  
def build_spice_setup_c():
    os.chdir(PROJECT_DIR)

    cmd = '"%s" Spice.sln /Rebuild "release|x86" /project %s' % (DEVENV, 'SetUp')
    run_command(cmd, logger)
    
    
def build_spice_setup_exe(project_dir):
    project = os.path.join(project_dir, 'Setup_VC')
    os.chdir(project)
    
    cmd = '"%s" Setup_VC.sln /Rebuild "release|win32" /project %s' % (DEVENV, 'SetUp_VC')
    run_command(cmd, logger)



def init_env(str, config_file): 

    new_lines = []
    with open(config_file, 'r') as f:
        lines = f.readlines()
        for line in lines:
            if 'AdditionalIncludeDirectories=' in line:
                line = '\t\t\t\t %s' % str
            new_lines.append(line)
    
    with open(config_file, 'w') as f1:
        for line in new_lines:
            f1.write(line)
             

def build_neoview():
    try:
        
        __init_environment()
#         clean(PROJECT_DIR)
           
        # init env
         
        logger.info('Begin init build environment')
        u2ec_str =  'AdditionalIncludeDirectories="%s"' % BOOT
        usb_str = 'AdditionalIncludeDirectories="%s;..\Consts;..\UsbService"' % BOOT
        u2ec_file = os.path.join(PROJECT_DIR,'u2ec\Application\u2ec\u2ec.vcproj')
        usb_file = os.path.join(PROJECT_DIR,'u2ec\Application\UsbService\UsbService.vcproj')
            
               
        usbservice_file=os.path.join(PROJECT_DIR,'u2ec\Application\UsbService\UsbService.vcproj')
         
        match='Release|Win32'
        attr='AdditionalLibraryDirectories'
        modify_value(usbservice_file,match,attr,WIN32_LIB)
       
        for match in ['Release|Win32','Release dll|Win32','Release|x64']:
            modify_value(u2ec_file,match,'AdditionalIncludeDirectories',r'C:\boost_1_50_0')
        for match in ['Release|Win32','Release|x86']:
            modify_value(usb_file,match,'AdditionalIncludeDirectories',r'%s;..\Consts;..\UsbService' % BOOT)
                  
        match='Release|x64'
        modify_value(usbservice_file,match,attr,WIN64_LIB)
        modify_value(usbservice_file,match,'AdditionalIncludeDirectories',r'%s;..\Consts;..\UsbService' % BOOT)
           
        logger.info('Finished build environment init.')
        logger.info(os.linesep)  
        
       
        #build spice
        spice_dir = os.path.join(PROJECT_DIR, 'bin', 'Release')
        build_spice(project_path=PROJECT_DIR, logger=logger, src=spice_dir, dist=RESOURCE, suffix='.exe')
        copy_restart_exe(spice_dir,RESOURCE)
        
        build_uninstall(project_path=PROJECT_DIR, logger=logger, src=spice_dir, dist=RESOURCE, suffix='.exe')
                
        # build redc
        redc_path = os.path.join(PROJECT_DIR, 'spicec\client\windows')
        redc_dir = os.path.join(PROJECT_DIR, 'spicec\client\windows\Release')
        build_redc(project_path=redc_path, logger=logger, src=redc_dir, dist=RESOURCE, suffix='.exe')    
          
        usb_service_dir = os.path.join(PROJECT_DIR, 'u2ec\Bin')
        # build u2ecdll
        u2ecdll_path = os.path.join(PROJECT_DIR, 'u2ec\Application')
        u2ecdll_dir = os.path.join(PROJECT_DIR, 'u2ec\Bin')
        build_u2ecdll(project_path=u2ecdll_path, logger=logger, src=usb_service_dir, dist=RESOURCE, suffix='.dll')      
            
        import time
        time.sleep(5)
                
        # build u2ec.lib
        u2eclib_path = os.path.join(PROJECT_DIR, 'u2ec\Application')
        build_u2eclib(project_path=u2eclib_path, logger=logger, src=usb_service_dir, dist=RESOURCE, suffix='.lib')
        # build usb
#          
        usb_serveice_path = os.path.join(PROJECT_DIR, 'u2ec\Application')
        build_usb_service(project_path=usb_serveice_path, logger=logger, src=usb_service_dir, dist=RESOURCE, suffix='.exe')   

        # build driver
#         base_dir = os.path.join(PROJECT_DIR, r'u2ec\Drivers')
#         build_driver(base_dir, WINDDK, RESOURCE)  
        
        setup_exe = os.path.join(PROJECT_DIR, 'bin', 'Release','SetUp.exe')
     
        setup_vc_dir=os.path.join(PROJECT_DIR, 'Setup_VC')
        build_spice_setup_c()
        shutil.copy(setup_exe, setup_vc_dir)
        build_spice_setup_exe(PROJECT_DIR)
        return True
    except Exception, ex:
        logger.exception(ex)
        sys.exit(1)
    
    

def create_zip():    
    logger.info('Begin create zip file.')
#     print 'Begin create zip file.'
    contents = ZIP_CONTENTS.replace('\n','').split(',')        
    name = os.path.join(CURDIR,'neoview.zip')
    
    f = zipfile.ZipFile(name, 'w' , zipfile.ZIP_DEFLATED) 
    for con in contents:
        ct = os.path.join(CURDIR,con)
        f.write(ct, os.path.basename(ct))
    f.close
#     print 'Build neoview successfull.'
    logger.info('Build neoview successfull.')
    sleep(10)
   
 
if __name__ == '__main__':
          
    bd = build_neoview()
    if bd == True:
           
        create_zip()
